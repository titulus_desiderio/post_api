const gulp = require('gulp');

require('shelljs/global');
const spawn = require('child_process').spawn
const psTree = require('ps-tree');

const kill = (pid, signal, callback) => {
    signal   = signal || 'SIGKILL';
    callback = callback || function () {};
    var killTree = true;
    if(killTree) {
        psTree(pid, function (err, children) {
            [pid].concat(
                children.map(function (p) {
                    return p.PID;
                })
            ).forEach(function (tpid) {
                try { process.kill(tpid, signal) }
                catch (ex) { }
            });
            callback();
        });
    } else {
        try { process.kill(pid, signal) }
        catch (ex) { }
        callback();
    }
};

let pid;
gulp.task('start', () => {
  console.log('Gulp: starting node');
  pid = exec('env DEBUG=api:* node index.js', {async: true});

  gulp.watch(['routes/**/*.js'], ['restart'])
    .on('change', (event) => {
      console.log('Gulp: File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('restart', () => {
  return new Promise(run => {
    console.log('Gulp: killing node');
    kill(pid.pid, null, run);
  })
    .then(() => {
      console.log('Gulp: starting node');
      pid = exec('env DEBUG=api:* node index.js', {async: true});
    })
});

gulp.task('default', ['start']);