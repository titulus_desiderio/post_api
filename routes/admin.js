var express = require('express');
var router = express.Router();
var fs = require('fs');
var moment = require('moment');

var CONFIG = require('../config.json');

/* GET users listing. */
router.all('/', function(req, res, next) {
  console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl}`);
  next();
});

router.get('/', function(req, res, next) {
  res.send({ok: true});
});


// COPIES down there
//TODO: replace with Factory

router.post('/groups-update', (req, res, next) => {
  var size = 0;
  var data = [];
  var id = Math.random().toString(36).slice(-8);
  console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}`);

  req.on('data', function (chunk) {
    size += chunk.length;
    data.push(chunk)
  });

  req.on('end', function () {
    console.info(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: end file transfer. ${size}b recieved`);
    data = Buffer.concat(data);
    var filename = 'admin/groups-update/groups.xml';
    if (size === 0) {
      console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Receive Error ${filename}`);
      console.error('Received 0 bytes');
      return res.status(400).send({ok: false, size, filename});
    }
    fs.writeFile(CONFIG.path+filename, data, function (error) {
      if (error) {
        console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Error ${filename}`);
        console.error(error);
        return res.status(500).send({ok: false, error, filename});
      }
      
      console.info(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Success ${filename}`);
      return res.send({ok: true, size, filename});
    });
  }); 

  req.on('error', function(e) {
    console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Recieve Error`);
    console.error(error);
    return res.status(500).send({ok: false, error});
  });
});

router.post('/add-action', (req, res, next) => {
  var size = 0;
  var data = [];
  var id = Math.random().toString(36).slice(-8);
  console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}`);

  req.on('data', function (chunk) {
    size += chunk.length;
    data.push(chunk)
  });

  req.on('end', function () {
    console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: end file transfer. ${size}b recieved`);
    data = Buffer.concat(data);
    // var filename = 'admin/add-action/newAction.xml';
    var filename = 'admin/add-action/'+moment().format("YYYYMMDDhhmmssSSS")+id+'.xml';
    if (size === 0) {
      console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Receive Error ${filename}`);
      console.error('Received 0 bytes');
      return res.status(400).send({ok: false, size, filename});
    }
    fs.writeFile(CONFIG.path+filename, data, function (error) {
      if (error) {
        console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Error ${filename}`);
        console.error(error);
        return res.status(500).send({ok: false, error, filename});
      }
      
      console.info(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Success ${filename}`);
      return res.send({ok: true, size, filename});
    });
  }); 

  req.on('error', function(e) {
    console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Recieve Error`);
    console.error(error);
    return res.status(500).send({ok: false, error});
  });
});

router.post('/addProduct-new', (req, res, next) => {
  var size = 0;
  var data = [];
  var id = Math.random().toString(36).slice(-8);
  console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}`);

  req.on('data', function (chunk) {
    size += chunk.length;
    data.push(chunk)
  });

  req.on('end', function () {
    console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: end file transfer. ${size}b recieved`);
    data = Buffer.concat(data);
    var filename = 'admin/addProduct-new/'+moment().format("YYYYMMDDhhmmssSSS")+id;
    if (size === 0) {
      console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Receive Error ${filename}`);
      console.error('Received 0 bytes');
      return res.status(400).send({ok: false, size, filename});
    }
    fs.writeFile(CONFIG.path+filename, data, function (error) {
      if (error) {
        console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Error ${filename}`);
        console.error(error);
        return res.status(500).send({ok: false, error, filename});
      }
      
      console.info(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Success ${filename}`);
      return res.send({ok: true, size, filename});
    });
  }); 

  req.on('error', function(e) {
    console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Recieve Error`);
    console.error(error);
    return res.status(500).send({ok: false, error});
  });
});

router.post('/add-klient', (req, res, next) => {
  var size = 0;
  var data = [];
  var id = Math.random().toString(36).slice(-8);
  console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}`);

  req.on('data', function (chunk) {
    size += chunk.length;
    data.push(chunk)
  });

  req.on('end', function () {
    console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: end file transfer. ${size}b recieved`);
    data = Buffer.concat(data);
    var filename = 'admin/add-klient/'+moment().format("YYYYMMDDhhmmssSSS")+id;
    if (size === 0) {
      console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Receive Error ${filename}`);
      console.error('Received 0 bytes');
      return res.status(400).send({ok: false, size, filename});
    }
    fs.writeFile(CONFIG.path+filename, data, function (error) {
      if (error) {
        console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Error ${filename}`);
        console.error(error);
        return res.status(500).send({ok: false, error, filename});
      }
      
      console.info(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Success ${filename}`);
      return res.send({ok: true, size, filename});
    });
  }); 

  req.on('error', function(e) {
    console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Recieve Error`);
    console.error(error);
    return res.status(500).send({ok: false, error});
  });
});

router.post('/addProduct/sopost-new', (req, res, next) => {
  var size = 0;
  var data = [];
  var id = Math.random().toString(36).slice(-8);
  console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}`);

  req.on('data', function (chunk) {
    size += chunk.length;
    data.push(chunk)
  });

  req.on('end', function () {
    console.log(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: end file transfer. ${size}b recieved`);
    data = Buffer.concat(data);
    var filename = 'admin/addProduct/sopost-new/'+moment().format("YYYYMMDDhhmmssSSS")+id;
    if (size === 0) {
      console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Receive Error ${filename}`);
      console.error('Received 0 bytes');
      return res.status(400).send({ok: false, size, filename});
    }
    fs.writeFile(CONFIG.path+filename, data, function (error) {
      if (error) {
        console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Error ${filename}`);
        console.error(error);
        return res.status(500).send({ok: false, error, filename});
      }
      
      console.info(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: FS: Success ${filename}`);
      return res.send({ok: true, size, filename});
    });
  }); 

  req.on('error', function(e) {
    console.warn(`${moment().format("YYYY.MM.DD hh:mm:ss")}: ${req.ip} ${req.method} ${req.originalUrl} ${id}: Recieve Error`);
    console.error(error);
    return res.status(500).send({ok: false, error});
  });
});

module.exports = router;
